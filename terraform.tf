provider "aws" {}

terraform {

  backend "s3" {
    bucket = "default"
    key    = "tfstate"
  }

  required_version = "~> 1.0.0"
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.59.0"
    }
  }
}
