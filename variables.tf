
variable "name" {
  type    = string
  default = "playground"
}

variable "vpc_cidr_block" {
  type    = string
  default = "10.0.0.0/22"
}

variable "tags" {
  type    = map(string)
  default = {}
}

variable "vpc_tags" {
  type    = map(string)
  default = {}
}

variable "subnet_public_tags" {
  type    = map(string)
  default = {}
}

variable "subnet_private_tags" {
  type    = map(string)
  default = {}
}


variable "instance_ami" {
  type    = string
  default = "ami-0d058fe428540cd89"
}

variable "instance_type" {
  type    = string
  default = "t2.medium"
}

variable "key_name" {
  type    = string
  default = ""
}

variable "iam_instance_profile" {
  type    = string
  default = ""
}

variable "ebs_optimized" {
  type    = bool
  default = false
}

variable "root_block_device" {
  type    = map(any)
  default = {}
}

variable "user_data" {
  type    = string
  default = null
}

variable "associate_public_ip_address" {
  type    = bool
  default = false
}

variable "desired_capacity" {
  type    = number
  default = 2
}

variable "min_instance_size" {
  type    = number
  default = 2
}

variable "max_instance_size" {
  type    = number
  default = 5
}

variable "extra_tags" {
  type = list(object({
    key                 = string
    value               = string
    propagate_at_launch = bool
  }))
  default = []
}
