# -----------------
# GATEWAY
# -----------------

resource "aws_internet_gateway" "this" {
  tags = merge(
    local.common_tags,
    tomap({ "Name" = lower(format("%s-igw", var.name)) }),
    tomap({ "Tier" = "internet-gateway" }),
  )

  vpc_id = aws_vpc.this.id
}

resource "aws_eip" "nat" {
  count = 1

  tags = merge(
    local.common_tags,
    tomap({ "Name" = lower(format("%s-natgw-eip", var.name)) }),
    tomap({ "Tier" = "nat-gateway" }),
  )

  vpc = true
}

resource "aws_nat_gateway" "this" {
  depends_on = [
    aws_internet_gateway.this,
    aws_eip.nat
  ]

  count = 1

  tags = merge(
    local.common_tags,
    tomap({ "Name" = lower(format("%s-natgw", var.name)) }),
    tomap({ "Tier" = "nat-gateway" }),
  )

  allocation_id = element(aws_eip.nat[*].id, count.index)
  subnet_id     = element(aws_subnet.public[*].id, count.index)
}
