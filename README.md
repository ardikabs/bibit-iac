# Description
Repository for Terraform Bibit Usecase

# [Prerequisites](./terraform.tf)
1. Terraform v1.0.0
2. AWS Provider 3.59.0

# How To
1. Please put the required environment variables on [.envrc](./envrc) file.
2. Populate the environment variables before running `terraform` command such the following:
    ```bash
    $ source .envrc
    ```
3. Because this sample using AWS Cloud Provider, don't forget to prepare your secret key and populate it also as environment variables with the following variable names:
    ```bash
    $ export AWS_SECRET_KEY=<aws-secret-key>
    $ export AWS_ACCESS_KEY_ID=<aws-access-key-id>
    ```
4. Running terraform
    ```bash
    $ terraform init
    $ terraform plan -out=default.tfplan
    $ terraform apply default.tfplan
    ```

# Output
The output on this sample Terraform project here are:
* 1 VPC with CIDR Block `10.0.0.0/22`
* 1 Public Subnet with 1 Internet Gateway
* 1 Private Subnet with 1 NAT Gateway that attached under public subnet
* 1 Elastic IP for the NAT Gateway
* 2 Route Table with respective usability for public and private subnet
* 1 Launch Configuration
* 1 Auto Scaling Group with 1 Auto Scaling Group Policy using Target Tracking Scaling based on Average CPU Utilization on 45%