# -----------------
# ROUTE TABLE
# -----------------

resource "aws_route_table" "public" {
  count = 1

  tags = merge(
    local.common_tags,
    tomap({ "Name" = lower(format("%s-public-routetable", var.name)) }),
    tomap({ "Tier" = "public" }),
  )

  vpc_id = aws_vpc.this.id
}

resource "aws_route" "public" {
  count                  = 1
  route_table_id         = aws_route_table.public[count.index].id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = aws_internet_gateway.this.id
}

resource "aws_route_table_association" "public" {
  count = 1

  subnet_id      = element(aws_subnet.public[*].id, count.index)
  route_table_id = aws_route_table.public[count.index].id
}

resource "aws_route_table" "private" {
  count = 1

  tags = merge(
    local.common_tags,
    tomap({ "Name" = lower(format("%s-private-routetable", var.name)) }),
    tomap({ "Tier" = "private" }),
  )

  vpc_id = aws_vpc.this.id
}

resource "aws_route" "private" {
  count                  = 1
  route_table_id         = aws_route_table.private[count.index].id
  destination_cidr_block = "0.0.0.0/0"
  nat_gateway_id         = aws_nat_gateway.this[count.index].id
}

resource "aws_route_table_association" "private" {
  count = 1

  subnet_id      = element(aws_subnet.private[*].id, count.index)
  route_table_id = aws_route_table.private[count.index].id
}

