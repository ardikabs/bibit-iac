locals {

  common_tags = merge(
    var.tags,
    tomap({ "ManagedBy" = "terraform" }),
  )

  asg_common_tags = [
    tomap({
      "key"                 = "ManagedBy",
      "value"               = "terraform",
      "propagate_at_launch" = true,
    })
  ]


  default_root_block_device = tomap({
    "delete_on_termination" = true,
    "encrypted"             = false,
    "volume_size"           = 15,
    "volume_type"           = "gp2",
  })
}

data "aws_availability_zones" "available" {
  state = "available"
}

# -----------------
# VPC
# -----------------

resource "aws_vpc" "this" {
  cidr_block = var.vpc_cidr_block

  enable_dns_support   = true
  enable_dns_hostnames = true

  tags = merge(
    local.common_tags,
    var.vpc_tags,
    tomap({ "Name" = lower(format("%s-vpc", var.name)) }),
  )
}

# -----------------
# SUBNET
# -----------------

resource "aws_subnet" "public" {
  count = 1

  tags = merge(
    local.common_tags,
    var.subnet_public_tags,
    tomap({ "Name" = lower(format("%s-public-subnet", var.name)) }),
    tomap({ "Tier" = "public" }),
  )

  vpc_id            = aws_vpc.this.id
  availability_zone = element(data.aws_availability_zones.available.names[*], count.index)

  cidr_block = cidrsubnet(var.vpc_cidr_block, 2, count.index)
}

resource "aws_subnet" "private" {
  count = 1

  tags = merge(
    local.common_tags,
    var.subnet_private_tags,
    tomap({ "Name" = lower(format("%s-private-subnet", var.name)) }),
    tomap({ "Tier" = "private" }),
  )

  vpc_id            = aws_vpc.this.id
  availability_zone = element(data.aws_availability_zones.available.names[*], count.index)

  cidr_block = cidrsubnet(var.vpc_cidr_block, 2, count.index + 1)
}


# --------------
# COMPUTE
# --------------

resource "aws_launch_configuration" "this" {
  count = 1
  name  = format("%s-launch-config", var.name)

  image_id             = var.instance_ami
  instance_type        = var.instance_type
  key_name             = var.key_name
  iam_instance_profile = var.iam_instance_profile

  associate_public_ip_address = var.associate_public_ip_address

  ebs_optimized = var.ebs_optimized

  root_block_device {
    delete_on_termination = lookup(merge(local.default_root_block_device, var.root_block_device), "delete_on_termination", null)
    encrypted             = lookup(merge(local.default_root_block_device, var.root_block_device), "encrypted", null)
    iops                  = lookup(merge(local.default_root_block_device, var.root_block_device), "iops", null)
    volume_size           = lookup(merge(local.default_root_block_device, var.root_block_device), "volume_size", null)
    volume_type           = lookup(merge(local.default_root_block_device, var.root_block_device), "volume_type", null)
  }

  user_data = var.user_data

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_autoscaling_group" "this" {
  count = 1

  tags = flatten([
    local.asg_common_tags,
    tomap({
      "key"                 = "Name",
      "value"               = format("%s-compute", var.name),
      "propagate_at_launch" = true,
    }),
    var.extra_tags,
  ])

  name             = format("%s-asg", var.name)
  desired_capacity = var.desired_capacity
  min_size         = var.min_instance_size
  max_size         = var.max_instance_size


  vpc_zone_identifier = [
    aws_subnet.private[count.index].id,
  ]
  launch_configuration = aws_launch_configuration.this[count.index].name

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_autoscaling_policy" "this" {
  name             = format("%s-scaling-by-cpu45", var.name)
  policy_type = "TargetTrackingScaling"
  target_tracking_configuration {
    predefined_metric_specification {
      predefined_metric_type = "ASGAverageCPUUtilization"
    }

    target_value = 45.0
  }

  autoscaling_group_name = aws_autoscaling_group.this[0].name
}
